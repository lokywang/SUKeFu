package com.ukefu.webim.web.model;

import javax.persistence.*;

@Entity
@Table(name = "uk_agentuser_extend")
@org.hibernate.annotations.Proxy(lazy = false)
public class AgentUserExtend implements java.io.Serializable {
    @Id
    @Column(name = "Id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVipGrade() {
        return vipGrade;
    }

    public void setVipGrade(String vipGrade) {
        this.vipGrade = vipGrade;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getIfVip() {
        return ifVip;
    }

    public void setIfVip(Boolean ifVip) {
        this.ifVip = ifVip;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Id
    private Integer id;
    private String userId;
    private String name;
    private Boolean ifVip=false;
    private String vipGrade;
    private String phone;
    private String address;
    private String region;
    private String email;
    private String sex;
    private String headPic;

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    private String registrationId;

    public String getHeadPic() {
        return headPic;
    }

    public void setHeadPic(String headPic) {
        this.headPic = headPic;
    }



}
