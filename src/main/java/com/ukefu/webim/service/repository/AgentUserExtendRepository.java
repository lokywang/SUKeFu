package com.ukefu.webim.service.repository;

import com.ukefu.webim.web.model.AgentUserExtend;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public abstract interface AgentUserExtendRepository extends JpaRepository<AgentUserExtend, Integer>
{

    public abstract AgentUserExtend findById(Integer id);
    public abstract AgentUserExtend findByUserId(String userid);
    public abstract Page<AgentUserExtend> findById(Integer id, Pageable paramPageable);
}